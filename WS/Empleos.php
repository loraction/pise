<?php
	
    require 'Database.php';

	class Registro{
		function _construct(){
		}
        
        public static function ObtenerEmpleoActual($usuario,$usuario2){
			$consultar = "SELECT trabajos.nombre as nombre, trabajos.empresa as nombreEmpresa, trabajos.inicio as inicio FROM trabajos WHERE id_alumno = ? and id = (select max(id) from trabajos where id_alumno = ?)";

			try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($usuario,$usuario2));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false; 

		}
                    
         public static function ObtenerUltimoEmpleo($usuario){
            $consultar = "SELECT trabajos.nombre as nombre, trabajos.empresa as nombreEmpresa, trabajos.inicio as inicio FROM trabajos WHERE trabajos.id_alumno = ? ORDER BY trabajos.id DESC LIMIT 1, 1";
            try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($usuario));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false;
            
            }   
        
        
            public static function NuevoEmpleo($nombre,$empresa,$id_area,$inicio,$id_alumno){
    
            $consultar = "INSERT INTO trabajos(nombre, empresa, id_area,  inicio, id_alumno,STATUS) VALUES (?,?,?,?,?,1)";
            try{
                $resultado = Database::getInstance()->getDb()->prepare($consultar);
                return $resultado->execute(array($nombre,$empresa,$id_area,$inicio,$id_alumno));
            }catch(PDOException $e){
                return false;
            }
            
            
        }
        
        
        
     
            

        
	}


?>