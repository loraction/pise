<?php
    require 'Login.php';
        
    if($_SERVER['REQUEST_METHOD']=='GET'){
        
        if(isset($_GET['id'])){
            $identificador = $_GET['id'];
            $Respuesta = Registro::ObtenerContactoPorID($identificador);
            $contenedor = array();
            if($Respuesta){
                $contenedor["datos"] = $Respuesta;
                echo json_encode($contenedor);  
            }else{
                echo json_encode(array('resultado' => 'El Usuario No Existe'));
            }
        }
    }
?>