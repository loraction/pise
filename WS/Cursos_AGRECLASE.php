<?php
    require 'Cursos.php';
    if($_SERVER['REQUEST_METHOD']=='POST'){
        $datos = json_decode(file_get_contents("php://input"),true);
        
        $respuesta = Registro::ObtenerCurso($datos["usuario"]);
        if($respuesta){
             echo json_encode(array('resultado' => 'Tienes Un Curso Activo'));
        }else{ 
            $respuesta = Registro::ObtenerDatosPorUsuario($datos["usuario"],$datos["clase"]);
            if($respuesta){
                echo json_encode(array('resultado' => 'Tienes este Curso en Espera'));
            }else{
                $respuesta = Registro::AgregarDatoClaseAlumno($datos["clase"],$datos["usuario"]);
                if($respuesta){
                    echo json_encode(array('resultado' => 'Se Agrego El Curso'));
                }else{ 
                    echo json_encode(array('resultado' => 'No se Agrego El Curso'));
                }
            }
        }
    }

?>