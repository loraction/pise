<?php
	
    require 'Database.php';

	class Registro{
		function _construct(){
		}

		public static function ObtenerTodosLosCursos(){
			$consultar = "SELECT cursos.id as id, cursos.nombre as nombre, universidades.nombre as univeridad, centros_investigacion.nombre as centros_investigacion FROM cursos INNER JOIN universidades ON universidades.id = cursos.id_universidad INNER JOIN centros_investigacion ON centros_investigacion.id = cursos.id_centro WHERE cursos.STATUS = 1";

			$resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute();

			$tabla = $resultado->fetchAll(PDO::FETCH_ASSOC);

			return ($tabla);

		}
            
         public static function ObtenerCurso($usuario){
            $consultar = "SELECT * FROM alumnos_cursos WHERE id_alumno = ?";
            try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($usuario));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false;
            
        }
        
        
        public static function ObtenerDatosPorUsuario($usuario, $clase){
            $consultar = "SELECT * FROM alumnos_cursos WHERE id_alumno = ? AND alumnos_cursos.id_curso = ?";
            try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($usuario, $clase));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false;
            
        }
        
        public static function ObtenerCursoAlumno($usuario){
            $consultar = "SELECT cursos.id as id, cursos.nombre as nombre, cursos.imparte as imparte, cursos.fecha as fecha, cursos.hora as hora, alumnos_cursos.STATUS as STATUS FROM alumnos_cursos INNER JOIN cursos ON cursos.id = alumnos_cursos.id_curso WHERE alumnos_cursos.id_alumno = ? AND alumnos_cursos.STATUS = 1";
            
            try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($usuario));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false;
            
        }
        
        public static function EliminarCurso($usuario,$curso){
            $consultar = "DELETE FROM alumnos_cursos WHERE alumnos_cursos.id_alumno = ? AND alumnos_cursos.id_curso = ?";
            try{
                $resultado = Database::getInstance()->getDb()->prepare($consultar);
                return $resultado->execute(array($usuario,$curso));
            }catch(PDOException $e){
                return false;
            }
            
        }
        
        public static function AgregarDatoClaseAlumno($clase,$usuario){
            $consultar = "INSERT INTO alumnos_cursos(id_curso,id_alumno,LOG, STATUS) VALUES (?,?,NULL,1)";
            try{
                $resultado = Database::getInstance()->getDb()->prepare($consultar);
                return $resultado->execute(array($clase,$usuario));
            }catch(PDOException $e){
                return false;
            }
            
        }
            

        
	}


?>