<?php
	
    require 'Database.php';

	class Registro{
		function _construct(){
		}

		public static function ObtenerTodosLasVacantes($area){
			$consultar = "SELECT vacantes.id as id, vacantes.nombre as nombre, vacantes.descripcion as descripcion, vacantes.inicio as inicio, vacantes.fin as fin, empresas.nombre as nombreEmpresa FROM vacantes INNER JOIN empresas ON vacantes.id_empresa INNER JOIN vacantes_areas ON vacantes_areas.id_vacante = vacantes.id WHERE vacantes_areas.id_area = ?";

			$resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($area));

			$tabla = $resultado->fetchAll(PDO::FETCH_ASSOC);

			return ($tabla);

		}
            
        public static function ObtenerDatosPorUsuario($usuario){
            $consultar = "SELECT * FROM usuarios WHERE usuario = ?";
            
            try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($usuario));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false;
            
        }
        
	}


?>