package net.com.lorajose.piseultimo.Vacantes;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.codingdemos.tablayout.R;

import net.com.lorajose.piseultimo.R;

public class Vacante extends AppCompatActivity {

    TextView lblPuesto;
    TextView lblRequisitos;
    TextView lblEmpresa;

    String PUESTO;
    String EMPRESA;
    String REQUISITOS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacante);

        lblPuesto = (TextView) findViewById(R.id.lblPuesto);
        lblEmpresa = (TextView) findViewById(R.id.lblEmpresa);
        lblRequisitos = (TextView) findViewById(R.id.lblRequisitos);

        Intent extras = getIntent();
        Bundle bundle = extras.getExtras();
        if(bundle!=null){
            PUESTO=bundle.getString("id");
            EMPRESA=bundle.getString("empresa");
            REQUISITOS=bundle.getString("descripcion");
        }

        lblPuesto.setText(PUESTO);
        lblEmpresa.setText(EMPRESA);
        lblRequisitos.setText(REQUISITOS);

    }
}
