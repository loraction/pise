package net.com.lorajose.piseultimo.Actividad;

import android.os.Build;
import android.os.Bundle;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.codingdemos.tablayout.R;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import net.com.lorajose.piseultimo.R;

public class Actividad extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    PageAdapterActividad pageAdapter;
    TabItem tabLaboral;
    TabItem tabPosgrado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Actividad");
        setSupportActionBar(toolbar);

        tabLayout = findViewById(R.id.tablayout);
        tabLaboral = findViewById(R.id.tabLaboral);
        tabPosgrado = findViewById(R.id.tabPosgrado);
        viewPager = findViewById(R.id.viewPager);

        pageAdapter = new PageAdapterActividad(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 1) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(Actividad.this, R.color.colorAccent));
                    tabLayout.setBackgroundColor(ContextCompat.getColor(Actividad.this, R.color.colorAccent));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(Actividad.this, R.color.colorAccent));
                    }
                } else if (tab.getPosition() == 2) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(Actividad.this,R.color.colorAccent));
                    tabLayout.setBackgroundColor(ContextCompat.getColor(Actividad.this,R.color.colorAccent));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(Actividad.this,R.color.colorAccent));
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }
}
