package net.com.lorajose.piseultimo.Cursos;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.com.lorajose.piseultimo.R;
import net.com.lorajose.piseultimo.VolleyRP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CatalogoFragment extends Fragment {

    private List<CursoConstructor> atributosList;
    private VolleyRP volley;
    private RequestQueue mRequest;
    private RecyclerView rv;
    private CursosAdapter adapter;
    String CARRERA;
    String IDCARRERA;
    String URL = "http://travelopolis.ddns.net/pise/Cursos_GETALL.php";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalogo, container, false);

        volley = VolleyRP.getInstance(getActivity());
        mRequest = volley.getRequestQueue();

        atributosList = new ArrayList<>();

        rv =(RecyclerView) view.findViewById(R.id.rvCursos);
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(lm);

        adapter = new CursosAdapter(atributosList,getContext());
        rv.setAdapter(adapter);

        solicitudJSON();


        return view;
    }

    public void solicitudJSON(){
        JsonObjectRequest solicitud = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject datos) {
                try {
                    String TodoslosDatos = datos.getString("resultado");
                    JSONArray jsonArray = new JSONArray(TodoslosDatos);
                    for (int i = 0; i<jsonArray.length();i++){
                        JSONObject js = jsonArray.getJSONObject(i);
                        agregarCurso(js.getString("id"),js.getString("nombre"),js.getString("univeridad"),
                                js.getString("centros_investigacion"));
                    }
                } catch (JSONException e) {
                    Toast.makeText(getActivity(),"Ocurrio un Error"+e, Toast.LENGTH_SHORT).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Ocurrio un Error"+error, Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,getActivity(),volley);
    }

    public void agregarCurso(String id, String nombre, String universidad, String centro){

        CursoConstructor curso = new CursoConstructor();
        curso.setId(id);
        curso.setNombre(nombre);
        curso.setUniversidad(universidad);
        curso.setCentro(centro);
        atributosList.add(curso);
        adapter.notifyDataSetChanged();

    }

}
